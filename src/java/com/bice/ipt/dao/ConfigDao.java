package com.bice.ipt.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.security.CodeSource;
import java.util.Properties;

public class ConfigDao {
	

	private static Properties obtenerDatosConexion()  {
		
		Properties properties = new Properties();
		properties.setProperty("username", "GRT_ADMIN");
		properties.setProperty("password", "GRT_ADMIN");
		properties.setProperty("url", "jdbc:oracle:thin:@10.110.1.54:1523:oraldes1");
		
		
		CodeSource codeSource = ConfigDao.class.getProtectionDomain().getCodeSource();
		try {
			
			File jarFile = new File(codeSource.getLocation().toURI().getPath());
			
			File dirLib = jarFile.getParentFile();
			
			File dirWebInf = dirLib.getParentFile();
		
			String pathProperties=dirWebInf.getAbsolutePath();
			pathProperties=pathProperties+"\\conf\\";
			File propFile = new File(pathProperties, "Garantias.properties");
			if(propFile.exists() && !propFile.isDirectory()) { 
				Properties prop = new Properties();
				prop.load(new BufferedReader(new FileReader(propFile.getAbsoluteFile())));
				String url= (String)prop.get("database.gar.url");
				System.out.println("[URL]->"+url);
				
				properties.setProperty("url",url);
				
				
				String clave= (String)prop.get("database.gar.password");
				System.out.println(clave);
			}
			
			
						
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
        
        
		return properties;
	}
	

}
