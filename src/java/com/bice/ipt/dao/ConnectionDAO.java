package com.bice.ipt.dao;

import java.sql.Connection;

import org.apache.turbine.services.db.TurbineDB;
import org.apache.turbine.util.db.pool.DBConnection;

import com.FHTServlet.modules.util.Constants;



public class ConnectionDAO  {

	

	private static String BD_TURBINE="memo";
	private static String BD_NO_TURBINE="jdbc:oracle:thin:@10.120.6.42:1521:ORACEEDES";
	
//	static final Logger LOGGER = Logger.getLogger(ConnectionDAO.class);
//	static final boolean IS_DEBUG = LOGGER.isDebugEnabled();
//	static final boolean IS_ERROR = LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR);
	
	
	public static DBConnection getConection(){
		boolean turbine=getUseTurbine();
		DBConnection dbconn = null;

		if(turbine){
			try{
			dbconn = TurbineDB.getConnection(Constants.BASE_GAR);
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
		return dbconn;
		
	}
	private static boolean getUseTurbine() {
		
//		//logger.log("----- getUseTurbine -------");
//		ResourceBundle rb = ResourceBundle.getBundle("com.bice.controlfinanciero.util.paramSistema");
//		//logger.log("----- "+rb+" -------");
//		boolean turb=false;
//		if(rb!=null){
//			String turbine= rb.getString("com.bice.controlfinanciero.conexion.turbine");
//			if(turbine.equalsIgnoreCase("true")){
//				turb=true;
//			}
//			//logger.log("-----Turbine["+turbine+"]------");
//			System.out.println("-----TurbineBool["+turb+"]------");
//		}

		return true;
	}
	public static void closeConnection(DBConnection dbconn,Connection conn) throws Exception{
	
		boolean turbine=getUseTurbine();
		System.out.println("-----Close Conecction : Turbine["+turbine+"]------");
		
		if(turbine){
			if(dbconn!=null){
				//logger.log("-----dbconn ditinto a nulo------");
				TurbineDB.releaseConnection(dbconn);
			}
		}else{
			if(conn!=null){
				//logger.log("-----conn ditinto a nulo------");
				conn.close();
			}
		}
		dbconn=null;
		conn=null;
	
	}
}
