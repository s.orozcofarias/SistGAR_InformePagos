package com.bice.ipt.dao;

import org.apache.turbine.util.db.pool.DBConnection;

import com.bice.ipt.dao.impl.InformePagoTurbineDAOImpl;


public class DaoFactory  {

//	final static Logger logger = Logger.getLogger(DaoFactory.class);
//	final static boolean isDebug = logger.isDebugEnabled();
//	final static boolean isError = logger.isEnabledFor(org.apache.log4j.Level.ERROR);
		
	private static DaoFactory instance;
	private DaoFactory(){}
	
	public static DaoFactory getInstance(){
		if(instance == null){
			instance = new DaoFactory();
		}
		return instance;
	}
	
	
	public InformePagoTurbineDAO getInformePagoTurbineDAO(DBConnection dbConnection){
		return new InformePagoTurbineDAOImpl(dbConnection);
	}
	
	
}