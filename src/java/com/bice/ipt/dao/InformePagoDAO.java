package com.bice.ipt.dao;

import java.util.List;

import com.bice.ipt.dao.entities.InformePago;
import com.bice.ipt.dao.entities.InformePagoFiltro;

public interface InformePagoDAO  {

	/**
	 * Metodo que retorna una lista de informes de pagos correspondientes a un periodo de tiempo
	 * @param inicio
	 * @param fin
	 * @return
	 */
	public List<InformePago> getInformePagoByFecha(InformePagoFiltro filtro);
}
