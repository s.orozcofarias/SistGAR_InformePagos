package com.bice.ipt.dao.entities;

import java.io.Serializable;
import java.util.Date;

public class InformePago implements Serializable {

	private static final long serialVersionUID = 1L;
	private Long rownum;
	private Long numeroGarantia;
	private Date fechaTasacion;
	private String rutCliente;
	private String dvCliente;
	private String nombreCliente;
	private String nombreEjecutivo;
	private String sucursal;
	private String tipoGarantia;
	private String calleCamino;
	private String tipoCalleCamino;
	private String numero;
	private String comuna;
	private Long numeroAvance;
	private String numeroCuenta;
	private String bancoBice;
	private String numeroBoleta;
	private Date fechaEmision;
	private Double montoCotizacion;
	private Double montoTasacionUf;
	private Double valorUf;
	private Double honorarios;
	private Double peaje;
	private Double kilometro;
	private Double otros;
	private String observaciones;
	private String codigoTasador;
	private String nombreTasador;
	private String gerencia;
	private String segmento;
	private String prodContable;
	private String cuentaCargo;
	
	public Long getNumeroGarantia() {
		return numeroGarantia;
	}
	public void setNumeroGarantia(Long numeroGarantia) {
		this.numeroGarantia = numeroGarantia;
	}
	public Date getFechaTasacion() {
		return fechaTasacion;
	}
	public void setFechaTasacion(Date fechaTasacion) {
		this.fechaTasacion = fechaTasacion;
	}
	public String getRutCliente() {
		return rutCliente;
	}
	public void setRutCliente(String rutCliente) {
		this.rutCliente = rutCliente;
	}
	public String getDvCliente() {
		return dvCliente;
	}
	public void setDvCliente(String dvCliente) {
		this.dvCliente = dvCliente;
	}
	public String getNombreCliente() {
		return nombreCliente;
	}
	public void setNombreCliente(String nombreCliente) {
		this.nombreCliente = nombreCliente;
	}
	public String getNombreEjecutivo() {
		return nombreEjecutivo;
	}
	public void setNombreEjecutivo(String nombreEjecutivo) {
		this.nombreEjecutivo = nombreEjecutivo;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}
	public String getTipoGarantia() {
		return tipoGarantia;
	}
	public void setTipoGarantia(String tipoGarantia) {
		this.tipoGarantia = tipoGarantia;
	}
	public String getCalleCamino() {
		return calleCamino;
	}
	public void setCalleCamino(String calleCamino) {
		this.calleCamino = calleCamino;
	}
	public String getTipoCalleCamino() {
		return tipoCalleCamino;
	}
	public void setTipoCalleCamino(String tipoCalleCamino) {
		this.tipoCalleCamino = tipoCalleCamino;
	}
	public String getNumero() {
		return numero;
	}
	public void setNumero(String numero) {
		this.numero = numero;
	}
	public String getComuna() {
		return comuna;
	}
	public void setComuna(String comuna) {
		this.comuna = comuna;
	}
	public Long getNumeroAvance() {
		return numeroAvance;
	}
	public void setNumeroAvance(Long numeroAvance) {
		this.numeroAvance = numeroAvance;
	}
	public String getNumeroCuenta() {
		return numeroCuenta;
	}
	public void setNumeroCuenta(String numeroCuenta) {
		this.numeroCuenta = numeroCuenta;
	}
	public String getBancoBice() {
		return bancoBice;
	}
	public void setBancoBice(String bancoBice) {
		this.bancoBice = bancoBice;
	}
	public String getNumeroBoleta() {
		return numeroBoleta;
	}
	public void setNumeroBoleta(String numeroBoleta) {
		this.numeroBoleta = numeroBoleta;
	}
	public Date getFechaEmision() {
		return fechaEmision;
	}
	public void setFechaEmision(Date fechaEmision) {
		this.fechaEmision = fechaEmision;
	}
	public Double getMontoCotizacion() {
		return montoCotizacion;
	}
	public void setMontoCotizacion(Double montoCotizacion) {
		this.montoCotizacion = montoCotizacion;
	}
	public Double getMontoTasacionUf() {
		return montoTasacionUf;
	}
	public void setMontoTasacionUf(Double montoTasacionUf) {
		this.montoTasacionUf = montoTasacionUf;
	}
	public Double getValorUf() {
		return valorUf;
	}
	public void setValorUf(Double valorUf) {
		this.valorUf = valorUf;
	}
	public Double getHonorarios() {
		return honorarios;
	}
	public void setHonorarios(Double honorarios) {
		this.honorarios = honorarios;
	}
	public Double getPeaje() {
		return peaje;
	}
	public void setPeaje(Double peaje) {
		this.peaje = peaje;
	}
	public Double getKilometro() {
		return kilometro;
	}
	public void setKilometro(Double kilometro) {
		this.kilometro = kilometro;
	}
	public Double getOtros() {
		return otros;
	}
	public void setOtros(Double otros) {
		this.otros = otros;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getCodigoTasador() {
		return codigoTasador;
	}
	public void setCodigoTasador(String codigoTasador) {
		this.codigoTasador = codigoTasador;
	}
	public String getNombreTasador() {
		return nombreTasador;
	}
	public void setNombreTasador(String nombreTasador) {
		this.nombreTasador = nombreTasador;
	}
	public Long getRownum() {
		return rownum;
	}
	public void setRownum(Long rownum) {
		this.rownum = rownum;
	}
	public String getGerencia() {
		return gerencia;
	}
	public void setGerencia(String gerencia) {
		this.gerencia = gerencia;
	}
	public String getSegmento() {
		return segmento;
	}
	public void setSegmento(String segmento) {
		this.segmento = segmento;
	}
	public String getProdContable() {
		return prodContable;
	}
	public void setProdContable(String prodContable) {
		this.prodContable = prodContable;
	}
	public String getCuentaCargo() {
		return cuentaCargo;
	}
	public void setCuentaCargo(String cuentaCargo) {
		this.cuentaCargo = cuentaCargo;
	}
	
	
	
}
