package com.bice.ipt.dao.entities;

public class InformePagoFiltro {

	private String fechaInicial;
	private String fechaTermino;
	private String ESTADO = "TERMI";
	private String FORMATO = "yyyyMMdd";
	private String usuario;
	
	public String getFechaInicial() {
		return fechaInicial;
	}
	public void setFechaInicial(String fechaInicial) {
		this.fechaInicial = fechaInicial;
	}
	public String getFechaTermino() {
		return fechaTermino;
	}
	public void setFechaTermino(String fechaTermino) {
		this.fechaTermino = fechaTermino;
	}
	public String getESTADO() {
		return ESTADO;
	}
	public String getFORMATO() {
		return FORMATO;
	}
	public void setESTADO(String eSTADO) {
		ESTADO = eSTADO;
	}
	public void setFORMATO(String fORMATO) {
		FORMATO = fORMATO;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	
	
	
	
}
