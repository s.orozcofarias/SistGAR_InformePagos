package com.bice.ipt.dao.impl;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.workingdogs.village.Record;

import oracle.jdbc.OracleConnection;
import oracle.jdbc.OracleTypes;

import org.apache.turbine.om.peer.BasePeer;
import org.apache.turbine.services.db.TurbineDB;
import org.apache.turbine.util.Log;
import org.apache.turbine.util.db.pool.DBConnection;

import com.FHTServlet.modules.util.Constants;
import com.bice.ipt.dao.InformePagoTurbineDAO;
import com.bice.ipt.dao.entities.InformePago;
import com.bice.ipt.dao.entities.InformePagoFiltro;

public class InformePagoTurbineDAOImpl implements InformePagoTurbineDAO {
	
	private static Connection conn = null;

	public InformePagoTurbineDAOImpl(DBConnection dbConnection) {
		// TODO Auto-generated constructor stub
		if(dbConnection!=null){
			try {
				conn=dbConnection.getConnection();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public List<InformePago> getInformePagoByFecha(InformePagoFiltro filtro) {
		// TODO Auto-generated method stub
		//String sqlText = getSelectInformePago(filtro);
		//Log.debug("InformePagoTurbineDAOImpl.getInformePagoByFecha: [" + sqlText + "]");
		
		String sp = "{call GRT_ADMIN.GRT_SP_CON_INFORME_PAGOS(?,?,?,?,?,?)}"; 
		Log.debug("sp : " + sp);
		CallableStatement cs  = null;
		ResultSet rs  = null;
		List<InformePago> listaInformes= new ArrayList<InformePago>();
		List lst;
		
		try {
	        			
			cs = conn.prepareCall(sp);

			cs.setString(1,filtro.getFechaInicial());
			cs.setString(2, filtro.getFechaTermino());
			cs.setString(3, filtro.getUsuario());
			cs.registerOutParameter(4, Types.NUMERIC);
			cs.registerOutParameter(5, Types.VARCHAR);
			cs.registerOutParameter(6, OracleTypes.CURSOR);
			cs.execute();
				       
			if (cs.getInt(4) == 0) {
				Log.debug("getInformePagoByFecha() EXITOSO ");
				rs = (ResultSet)  cs.getObject(6);
				
				while (rs.next()) {
					
					InformePago informePago = new InformePago();
					
		             informePago.setRownum(rs.getLong("ROWNUM"));
		             informePago.setNumeroGarantia(rs.getLong("GTI_NCN"));
		             informePago.setFechaTasacion(rs.getDate("GTI_FTS"));
		             informePago.setRutCliente(rs.getString("GTI_RCL"));
		             informePago.setNombreCliente(rs.getString("GTI_CLI"));
		             informePago.setNombreEjecutivo(rs.getString("GTI_NEJ"));
		             informePago.setSucursal(rs.getString("GTI_NSU"));
		             informePago.setGerencia(rs.getString("GERENCIA"));
		             informePago.setSegmento(rs.getString("SEGMENTO"));
		             informePago.setTipoGarantia(rs.getString("GTI_DCN"));
		             informePago.setProdContable(rs.getString("PROD_CONTABLE"));
		             informePago.setCalleCamino(rs.getString("DIR_COC"));
		             informePago.setTipoCalleCamino(rs.getString("DIR_TCC"));
		             informePago.setNumero(rs.getString("DIR_NUM"));
		             informePago.setComuna(rs.getString("DIR_CMN"));
		             informePago.setNumeroAvance(rs.getLong("HAV_NAV"));
		             informePago.setCuentaCargo(rs.getString("CTA_CARGO"));
		             informePago.setNumeroCuenta(rs.getString("GTI_CCT"));
		             informePago.setBancoBice(rs.getString("BANCO_BICE"));
		             informePago.setNumeroBoleta(rs.getString("GTI_NFA"));
		             informePago.setFechaEmision(rs.getDate("SRC_FIN"));
		             informePago.setMontoCotizacion(rs.getDouble("MONTO_COTIZACION"));
		             informePago.setMontoTasacionUf(rs.getDouble("MTO_TAS_UF"));           
		             //informePago.setValorUf(rec.getValue("GTI_VUF").asDouble());
		             informePago.setHonorarios(rs.getDouble("HONORARIOS"));
		             //informePago.setPeaje(rec.getValue("PEAJE").asDouble());
		             //informePago.setKilometro(rec.getValue("KILOMETROS").asDouble());
		             informePago.setObservaciones(rs.getString("OBSERVACIONES"));
		             informePago.setCodigoTasador(rs.getString("GTI_NIT"));
		             informePago.setNombreTasador(rs.getString("GTI_NOT"));
		             informePago.setOtros(rs.getDouble("GASTOS"));
		    		
		             listaInformes.add(informePago);
		             
				}
			} else {
				Log.debug("ERROR : " + cs.getInt(5));
			}
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				cs.close();
			    rs.close();
			    conn.close();
			 }
			 catch (SQLException ee){
				 ee.printStackTrace(); 
			 }
		}     
		return listaInformes;
	}

	
	/*
	private String getSelectInformePago(InformePagoFiltro filtro) {
		// TODO Auto-generated method stub
		
		StringBuilder sb= new  StringBuilder();
		sb.append(" SELECT ");
		sb.append(" ROWNUM,");
		sb.append(" GTIA.GTI_NCN,");
		sb.append(" GTIA.GTI_FTS,");
		sb.append(" GTIA.GTI_RCL,");
		sb.append(" GTIA.GTI_CLI,");
		sb.append(" GTIA.GTI_NEJ,");
		sb.append(" GTIA.GTI_NSU,");
		sb.append("InitCap(GLS_GERENCIACORTA) AS GERENCIA,");
		sb.append("InitCap(gls_classegmen_corta) AS SEGMENTO,");
		sb.append(" GTIA.GTI_DCN,");
		sb.append(" DIR.DIR_COC,");
		sb.append(" DIR.DIR_TCC,");
		sb.append(" DIR.DIR_NUM,");
		sb.append(" COMUNA.CMN_DSC DIR_CMN,");
		sb.append(" HAV.HAV_NAV,");
		sb.append(" GTIA.GTI_CCT,");
		sb.append(" '' BANCO_BICE,");
		sb.append(" GTIA.GTI_NFA,");
		sb.append(" HONO.DVC_NDT, ");
		sb.append(" SRC.SRC_FIN, ");
		sb.append(" 0 MONTO_COTIZACION,");
		sb.append(" (GTIA.GTI_VTS / GTIA.GTI_VUF) MTO_TAS_UF,");
		sb.append(" HONO.DVC_MNT HONORARIOS,");
		sb.append(" GASTOS.GASTOS GASTOS,");
		sb.append(" '' OBSERVACIONES,");
		sb.append(" GTIA.GTI_NIT,");
		sb.append(" GTIA.GTI_NOT");
		sb.append(" FROM ");
		sb.append(" GRT_ADMIN.TT_GTI_TRA GTIA ");
		sb.append(" LEFT JOIN GRT_ADMIN.TT_DIR_TRA DIR ON(DIR.DIR_NCN = GTIA.GTI_NCN AND DIR.DIR_NEV = GTIA.GTI_NEV)");
		sb.append(" LEFT JOIN GRT_ADMIN.TT_HAV_TRA HAV ON(HAV.HAV_NCN = GTIA.GTI_NCN AND HAV.HAV_NEV = GTIA.GTI_NEV)");
		sb.append(" LEFT JOIN GRT_ADMIN.TT_DVC_TRA HONO ON(HONO.DVC_NCN = GTIA.GTI_NCN AND HONO.DVC_NEV = GTIA.GTI_NEV AND HONO.VCT_NID in (SELECT VCT_NID from GRT_ADMIN.TT_VCT_TRA WHERE VCT_DTL ='Honorarios'))");
		sb.append(" LEFT JOIN (SELECT DVC.DVC_NCN NCN, DVC.DVC_NEV NEV, SUM(DVC.DVC_MNT) GASTOS FROM GRT_ADMIN.TT_DVC_TRA DVC WHERE DVC.VCT_NID NOT IN (SELECT VCT_NID from GRT_ADMIN.TT_VCT_TRA WHERE VCT_DTL ='Honorarios') GROUP BY DVC.DVC_NCN, DVC.DVC_NEV) GASTOS ON GTIA.GTI_NCN = GASTOS.NCN AND GTIA.GTI_NEV = GASTOS.NEV");
		sb.append(" LEFT JOIN GRT_ADMIN.TT_DES_TRA DES ON(DES.DES_NCN = GTIA.GTI_NCN AND DES.DES_NEV = GTIA.GTI_NEV)");
		sb.append(" LEFT JOIN GRT_ADMIN.TT_SRC_TRA SRC ON (DES.DES_NID = SRC.SRC_FID)");
		sb.append(" LEFT JOIN GRT_ADMIN.TT_CMN COMUNA ON ( (COMUNA.CMN_PRV||COMUNA.CMN_COD) = DIR.DIR_CMN and COMUNA.CMN_PRV = DIR.DIR_PRV and COMUNA.CMN_RGN = DIR.DIR_REG)");
		sb.append(" LEFT JOIN CIF_VIEW_CLIENTES V ON (TO_NUMBER(SUBSTR(TRIM(GTIA.GTI_RCL),1,LENGTH(TRIM(GTIA.GTI_RCL))-1))= TO_NUMBER(SUBSTR(TRIM(V.NUM_IDEN),1,LENGTH(TRIM(V.NUM_IDEN))-1)))");
		sb.append(" LEFT JOIN BCT_VIEW_EJECTA   E ON (V.NUM_ROL_EMPLEADO = E.NUM_ROLEMPLEADO AND V.COD_SUCURSAL = E.COD_SUCURSAL )");
		sb.append(" WHERE ");
		sb.append(" GTIA.GTI_FTS BETWEEN TO_DATE("+filtro.getFechaInicial()+",'"+filtro.getFORMATO()+"') AND TO_DATE("+filtro.getFechaTermino()+",'"+filtro.getFORMATO()+"')");
		sb.append(" AND ");
		sb.append(" GTIA.GTI_ES2 = '"+filtro.getESTADO()+"'");
		
		return sb.toString();
	}
	*/

}
