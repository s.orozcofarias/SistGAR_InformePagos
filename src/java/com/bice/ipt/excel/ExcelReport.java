package com.bice.ipt.excel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Map;








import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import com.bice.ipt.exception.ReporteriaException;
import com.bice.ipt.type.ReporteriaExcelEnum;
import com.bice.ipt.util.StringUtil;

public class ExcelReport   {

//	final static Logger logger = Logger.getLogger(ExcelReport.class);
//	final static boolean isDebug = logger.isDebugEnabled();
//	final static boolean isError = logger.isEnabledFor(org.apache.log4j.Level.ERROR);
	
	/**
	 * Metodo que genera excel y lo envia dentro de un ouputStream de Servlet para descargar 
	 * @param outputStream
	 * @param map
	 * @throws ReporteriaException
	 */
	public void generaReporteExcel(File file, Map<String, Object> map) throws ReporteriaException{
		/*Se crea el libro de excel usando el objeto de tipo Workbook*/
		HSSFWorkbook libro = null;
        FileOutputStream out = null;
		try {
			if(null != file && map != null && map.containsKey(ReporteriaExcelEnum.TITULO_COLUMNA.toString()) && map.containsKey(ReporteriaExcelEnum.NOMBRE_HOJA.toString()) ){
				
				libro = new HSSFWorkbook();
		        /*Utilizamos la clase Sheet para crear una nueva hoja de trabajo dentro del libro que creamos anteriormente*/
				HSSFSheet hoja = libro.createSheet((String) map.get(ReporteriaExcelEnum.NOMBRE_HOJA.toString()));
		        int fila = 0;
		        short celda = 0;
		        
		        HSSFCell cell = null;
		        
		        //crea fila y celdas iniciales para hoja de proceso de evaluación de servicios 
		        HSSFRow row = hoja.createRow(0);
		        List<String> listaTexto = null;
		        if(map.get(ReporteriaExcelEnum.TITULO_COLUMNA.toString()) instanceof List){
		        	listaTexto =(List<String>) map.get(ReporteriaExcelEnum.TITULO_COLUMNA.toString());
		        }
		        
		        if(listaTexto != null){
		        	for (String titulo : listaTexto) {
		        		//nueva celda con titulo dinamico
		        		cell = row.createCell(celda++);
				        cell.setCellValue(titulo);
					}
		        	listaTexto = null;
		        }
		        
		        //remueve la lista de titulos de columna y el nombre de la hoja, deja solo las listas con información
		        map.remove(ReporteriaExcelEnum.NOMBRE_HOJA.toString());
		        map.remove(ReporteriaExcelEnum.TITULO_COLUMNA.toString());
		        
		        //inicia en la primera celda a la izquierda
		        celda = 0;
		        
		        //short maxLeng=0;
		        
		        if(!map.isEmpty()){
//		        	int index = 0;
			        //recorre los componentes que quedan dentro del map
		        	for (int i = 0; i < map.values().size(); i++) {
		        		Object lista = map.get(""+i);
		        		if(lista != null && lista instanceof List){
			        		listaTexto = (List<String>) lista;
			        	}
		        		if(listaTexto != null){
		        			//crea una fila y agrega una nueva celda dentro de ella
			        		row = hoja.createRow(++fila);
			        		//logger.debug("fila "+fila +" rut listaTexto "+listaTexto.get(5));
			        		for (String texto : listaTexto) {
						    	//celda con valor del rut del proveedor
					        	cell = row.createCell(celda++);			        	
					        	if(StringUtil.isNumero(texto)){
					        		cell.setCellValue(new Double(texto));
					        	}else{
					        		cell.setCellValue(texto);
					        	}
					        	
//					        	if(texto.length()> maxLeng){
//					        		maxLeng=(short) texto.length();
//					        	}
//					        	hoja.setColumnWidth( (short) (row.getRowNum()*10), maxLeng);
					        	
					        	
					        	//hoja.autoSizeColumn(cell.getColumnIndex());
					        	//deja celda en 0, para crear desde el primer indice nuevamente
					       }
			        		celda = 0;
			        	}
			        	else{
			        		//libro.close();
			        		throw new ReporteriaException("La lista con texto se encuentra null ");
			        	}
			        }
				}
		        /*Escribimos en el libro*/
		        out = new FileOutputStream(file);
		        libro.write(out);
		        
		   }
		} catch (IOException e) {
			
				throw new ReporteriaException("Error IOException de generaReporteExcelByWeb en AbstractExcelReport ",e);
			
		} 
		finally{
			try {
				if(out != null){
					out.close();
				}
				if(libro != null){
					//libro.close();
				}
			} catch (IOException e) {
				
					throw new ReporteriaException("Error IOException de generaReporteExcelByWeb en AbstractExcelReport, al cerrar el libro y el output ",e);
				
			}
		}
	}
}
