package com.bice.ipt.excel;

import java.io.File;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;





import com.bice.ipt.dao.ConfigDao;
import com.bice.ipt.dao.ConnectionDAO;
import com.bice.ipt.dao.DaoFactory;
import com.bice.ipt.dao.InformePagoDAO;
import com.bice.ipt.dao.InformePagoTurbineDAO;
import com.bice.ipt.dao.entities.InformePago;
import com.bice.ipt.dao.entities.InformePagoFiltro;
import com.bice.ipt.exception.ReporteriaException;
import com.bice.ipt.type.InformePagoTituloEnum;
import com.bice.ipt.type.ReporteriaExcelEnum;

import com.bice.ipt.util.StringUtil;

import org.apache.turbine.util.Log;
import org.apache.turbine.util.db.pool.DBConnection;

public class InformePagoTasadorExcel implements Serializable {

	private static final long serialVersionUID = 1L;
//	static final Logger LOGGER = Logger
//			.getLogger(InformePagoTasadorExcel.class);
//	static final boolean IS_DEBUG = LOGGER.isDebugEnabled();
//	static final boolean IS_ERROR = LOGGER
//			.isEnabledFor(org.apache.log4j.Level.ERROR);
	static final String NOMBRE_HOJA = "Informe Pago Tasador";
	static final String NOMBRE_ARCHIVO = "Informe Pago Tasadores.xls";
	
	private InformePagoFiltro filtro = null;

	public File generaInformePagoTasador(Date fechaInicio, Date fechaFin, String usuario)
			throws Exception {
		File archivo = null;

		Map<String, Object> excelMap = new HashMap<String, Object>();
		List<String> titulos = new ArrayList<String>();
		cargaTitulosTabla(excelMap, titulos);

		excelMap.put(ReporteriaExcelEnum.TITULO_COLUMNA.toString(), titulos);
		excelMap.put(ReporteriaExcelEnum.NOMBRE_HOJA.toString(), NOMBRE_HOJA);

//		InformePagoDAO informePagoDAO = DaoFactory.getInstance()
//				.getInformePagoDAO(ConfigDao.getSession());
		
		DBConnection dbconn=ConnectionDAO.getConection();
		
		InformePagoTurbineDAO informePagoDAO=  DaoFactory.getInstance().getInformePagoTurbineDAO(dbconn);
		
		if (informePagoDAO != null) {
			//ConfigDao.abrirConexion();
			filtro = getFiltro(fechaInicio, fechaFin, usuario);
			List<InformePago> informesPago = informePagoDAO.getInformePagoByFecha(filtro);
			
			int index = 0;

			if (informesPago != null) {
				for (InformePago informe : informesPago) {
					List<String> listaTexto = null;
					if (informe != null) {
						listaTexto = getInformeRow(informe);
						excelMap.put("" + index++, listaTexto);
					}
				}
			}
			ConnectionDAO.closeConnection(dbconn, null);
			try {
				ExcelReport excelReport = new ExcelReport();
				archivo = new File(	NOMBRE_ARCHIVO);
				excelReport.generaReporteExcel(archivo, excelMap);
			} catch (ReporteriaException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		return archivo;
	}

	private void cargaTitulosTabla(Map<String, Object> excelMap,
			List<String> titulos) {
		for (InformePagoTituloEnum informeEnum : InformePagoTituloEnum.values()) {
			if (informeEnum != null) {
				if (informeEnum.toString().contains("TITULO_")) {
					titulos.add(informeEnum.getLabel());
					//System.out.println(excelMap.get(informeEnum.toString()));
				}
			}
		}
	}

	private List<String> getInformeRow(InformePago informe) throws Exception {
		List<String> listaTexto;
		StringBuilder direccion;
		Double montoTasacion;
		Double gastos;
		String rut = null;
		String dv = null;
		listaTexto = new ArrayList<String>();
		listaTexto.add(getStringValue(informe.getRownum()));
		listaTexto.add(getStringValue(informe.getNumeroGarantia()));
		listaTexto.add(StringUtil.convertDateToDDMMYYYString(informe
				.getFechaTasacion()));
		// divide el rut
		if (informe.getRutCliente() != null) {
			String[] rutArray = informe.getRutCliente().split("-");
			if (rutArray != null && rutArray.length > 1) {
				rut = rutArray[0].replace(".", "");
				dv = rutArray[rutArray.length - 1];
			}else{
				rut=informe.getRutCliente().substring(0, informe.getRutCliente().length()-1);
				dv=informe.getRutCliente().charAt(informe.getRutCliente().length()-1)+"";
			}
			
		}
		listaTexto.add(getStringValue(rut));
		listaTexto.add(getStringValue(dv));
		listaTexto.add(getStringValue(informe.getNombreCliente()));
		listaTexto.add(getStringValue(informe.getNombreEjecutivo()));
		listaTexto.add(getStringValue(informe.getSucursal()));
		listaTexto.add(getStringValue(informe.getGerencia()));
		listaTexto.add(getStringValue(informe.getSegmento()));
		listaTexto.add(getStringValue(informe.getTipoGarantia()));
		listaTexto.add(getStringValue(informe.getProdContable()));

		// arma direccion y la agrega a la lista
		direccion = new StringBuilder();
		direccion.append(getStringValue(informe.getCalleCamino()));
		direccion.append(" ");
		direccion.append(getStringValue(informe.getTipoCalleCamino()));
		direccion.append(" No ");
		direccion.append(getStringValue(informe.getNumero()));
		listaTexto.add(getStringValue(direccion.toString()));

		listaTexto.add(getStringValue(informe.getComuna()));
		listaTexto.add(getStringValue(informe.getNumeroAvance()));
		listaTexto.add(getStringValue(informe.getCuentaCargo()));
		listaTexto.add(getStringValue(informe.getNumeroCuenta()));
		listaTexto.add(getStringValue(informe.getBancoBice()));
		listaTexto.add(getStringValue(informe.getNumeroBoleta()));
		listaTexto.add(getStringValue(informe.getFechaEmision()));
		listaTexto.add(getStringValue(informe.getMontoCotizacion()));
		listaTexto.add(getStringValue(informe.getMontoTasacionUf()));
		
		// calcula el monto de tasacion en pesos
		/*Se comenta esta conversion porque no es necesaria y ademas no esta trayendo el valor de la UF*/
		
		//montoTasacion = calculaMontoTasacion(informe);
		//istaTexto.add(getStringValue(montoTasacion));
		
		listaTexto.add(getStringValue(informe.getHonorarios()));
		// calcula los gastos y los agrega a la lista
		gastos = new Double(0);
		gastos += informe.getOtros() != null ? informe.getOtros() : 0L;
		listaTexto.add(getStringValue(gastos));
		Double honorarios=informe.getHonorarios();
		honorarios=honorarios==null?0d:honorarios;
		listaTexto.add(getStringValue(gastos + honorarios));

		listaTexto.add(getStringValue(informe.getObservaciones()));
		listaTexto.add(getStringValue(informe.getCodigoTasador()));
		listaTexto.add(getStringValue(informe.getNombreTasador()));
		return listaTexto;
	}

	private Double calculaMontoTasacion(InformePago informe) {
		Double montoTasacion;
		if (informe.getMontoTasacionUf() != null
				&& informe.getValorUf() != null) {
			montoTasacion = new Double(informe.getMontoTasacionUf()
					* informe.getValorUf());
		} else {
			montoTasacion = new Double(0);
		}
		return montoTasacion;
	}

	private String getStringValue(Object o) {
		return String.valueOf(o == null ? "" : o);
	}

	private InformePagoFiltro getFiltro(Date fechaInicio, Date fechaFin, String usuario)
			throws Exception {
		String fechaInicioStr = StringUtil
				.convertDateToYYYYMMDDString(fechaInicio);
		String fechaFinStr = StringUtil.convertDateToYYYYMMDDString(fechaFin);
		
		getFiltro().setFechaInicial(fechaInicioStr);
		getFiltro().setFechaTermino(fechaFinStr);
		getFiltro().setUsuario(usuario);
		return getFiltro();
	}
	
	public void cambiaFiltroEstadoGtia(String estado){
		getFiltro().setESTADO(estado);
	}
	public void cambiaFiltroFormtato(String formato){
		getFiltro().setFORMATO(formato);
		
	}
	public InformePagoFiltro getFiltro() {
		if(filtro == null){
			filtro = new InformePagoFiltro();
		}
		return filtro;
	}

	public void setFiltro(InformePagoFiltro filtro) {
		this.filtro = filtro;
	}

}
