package com.bice.ipt.exception;

import java.io.Serializable;

public class ReporteriaException extends Exception implements Serializable{

	private static final long serialVersionUID = 1L;

	public ReporteriaException()  {
		super();
		// TODO Auto-generated constructor stub
	}

	public ReporteriaException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public ReporteriaException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}
	

}
