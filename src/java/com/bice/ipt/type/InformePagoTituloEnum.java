package com.bice.ipt.type;

/**
 * 
 * @author alexis.osorio
 * Enum que contiene el nombre de cada titulo del informe de pago de tasadores, ademas 
 * se muestra en este mismo orden 
 */
public enum InformePagoTituloEnum {
	
	TITULO_INDICE("#"),
	TITULO_NUMERO_GARANTIA("No Garantia"),
	TITULO_FECHA_TASADOR("Fecha de Tasacion"),
	TITULO_RUT_CLIENTE("RUT del cliente"),
	TITULO_DV_CLIENTE("Dv"),
	TITULO_NOMBRE_CLIENTE("Nombre Cliente"),
	TITULO_NOMBRE_EJECUTIVO("Nombre Ejecutivo"),
	TITULO_SUCURSAL("Sucursal O Area"),
	TITULO_GENERENCIA("Gerencia"),
	TITULO_SEGMENTO("Segmento"),
	TITULO_TIPO_GARANTIA("CRHIP / HIPRD"),
	TITULO_PROD_CONTABLE("Producto Contable"),
	TITULO_DIRECCION("DIRECCION o Nombre Proyecto"),
	TITULO_COMUNA("COMUNA"),
	TITULO_NUMERO_AVANCE("No Avance"),
	TITULO_CUENTA_CARGO("Cuenta Cargo"),
	TITULO_NUMERO_CUENTA("A Nombre Banco Bice / Num Cta Cte"),
	TITULO_BANCO_BICE("Banco Bice"),
	TITULO_NUMERO_BOLETA("No Boleta / Factura"),
	TITULO_FECHA_EMISION("Fecha de emision [dd/mm/aa]"),
	TITULO_MONTO_COTIZACION("Monto Cotizacion (si hubo) [UF]"),
	TITULO_MONTO_TASACION_UF("Monto de Tasacion [UF]"),
	TITULO_HONORARIOS("Honorarios [$]"),
//	TITULO_PEAJE("Gastos [$]"),
//	TITULO_KILOMETRO("Gastos [$]"),
	TITULO_OTROS("Gastos [$]"),
	TITULO_TOTAL("Total [$]"),
	TITULO_OBSERVACIONES("Observaciones"),
	TITULO_CODIGO_TASADOR("Codigo Tasador"),
	TITULO_NOMBRE_TASADOR("Nombre Tasador");
	
	private String label;
	
	private InformePagoTituloEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public static InformePagoTituloEnum getEnumByString(String code){
		if(code != null){
			for(InformePagoTituloEnum e : InformePagoTituloEnum.values()){
				if(code.equals(e.label)){
					return e;
				}
			}
		}
		return null;
	}
}
