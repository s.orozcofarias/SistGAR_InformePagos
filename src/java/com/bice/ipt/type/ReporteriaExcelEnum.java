package com.bice.ipt.type;

/**
 * 
 * @author alexis.osorio
 *	Enum con constantes unicas de cualquier metodo de conversion a excel
 */
public enum ReporteriaExcelEnum  {

	/**
	 * Proceso de evaluacion de servicio
	 */
	TITULO_COLUMNA("Titulos de columnas"),
	NOMBRE_HOJA("Nombre Hoja Excel")
	;
	
	private String label;
	
	private ReporteriaExcelEnum(String label) {
		this.label = label;
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	public static ReporteriaExcelEnum getEnumByString(String code){
		if(code != null){
			for(ReporteriaExcelEnum e : ReporteriaExcelEnum.values()){
				if(code.equals(e.label)){
					return e;
				}
			}
		}
		return null;
	}
}
