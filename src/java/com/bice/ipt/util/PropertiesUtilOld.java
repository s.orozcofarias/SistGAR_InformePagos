package com.bice.ipt.util;

import java.util.MissingResourceException;
import java.util.ResourceBundle;


public class PropertiesUtilOld {

	private static PropertiesUtilOld INSTANCE = null;
	private ResourceBundle recursos = null;
	private final String PROPERTY_NAME = "aplicacion";
	
//	final static Logger logger = Logger.getLogger(PropertiesUtil.class);
//	final static boolean isError = logger.isEnabledFor(org.apache.log4j.Level.ERROR);
	
	private PropertiesUtilOld() {}
	
	public static PropertiesUtilOld getInstance(){
		if(INSTANCE == null){
			return new PropertiesUtilOld();
		}
		return INSTANCE;
	}
	
	public String getString(String name){
		try{
			if(recursos == null){
				recursos = ResourceBundle.getBundle(PROPERTY_NAME);
			}
			return recursos.getString(name);
		}catch(MissingResourceException e){
			
				System.out.println("\n\nNO ES POSIBLE ENCONTRAR LA PROPIEDAD ["+name+"]");
				System.out.println("MissingResourceException "+e);
			
		}	
		return null;
		
	}
	
	
}
