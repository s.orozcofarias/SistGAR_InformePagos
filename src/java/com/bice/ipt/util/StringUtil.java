package com.bice.ipt.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtil  {

	private final static String RE_ONLY_NUMBERS = "^\\d+$";
	private final static String RE_VALID_EMAIL = "^((([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+(\\.([a-z]|\\d|[!#\\$%&'\\*\\+\\-\\/=\\?\\^_`{\\|}~]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])+)*)|((\\x22)((((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(([\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x7f]|\\x21|[\\x23-\\x5b]|[\\x5d-\\x7e]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(\\\\([\\x01-\\x09\\x0b\\x0c\\x0d-\\x7f]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF]))))*(((\\x20|\\x09)*(\\x0d\\x0a))?(\\x20|\\x09)+)?(\\x22)))@((([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|\\d|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))\\.)+(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])|(([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])([a-z]|\\d|-|\\.|_|~|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])*([a-z]|[\\u00A0-\\uD7FF\\uF900-\\uFDCF\\uFDF0-\\uFFEF])))$";
	
	/***
	 * Metodo que recibe un String y retorna true si solo tiene numeros 
	 * @param number
	 * @return boolean
	 */
	public static boolean isNumero(String number){
		
		if(number != null){
			Pattern pat = Pattern.compile(RE_ONLY_NUMBERS);  
			Matcher mat = pat.matcher(number);
	        return mat.matches();
		}
		return false;
	} 
	/**
	 * Metodo que valida si un email tiene un formato valido mediante una expresion regular
	 * @param email
	 * @return
	 */
	public static boolean emailValido(String email){
		if(email != null){
			Pattern pat = Pattern.compile(RE_VALID_EMAIL);  
			Matcher mat = pat.matcher(email);
	        return mat.matches();
		}
		return false;
	}
	
	/**
	 * Metodo que recibe el nombre de un archivo con su extension y el nuevo nombre que tendra el archivo, retorna el nuevo nombre 
	 * con la extension recibida
	 * @param nombre
	 * @param archivoExt
	 * @return
	 */
	public static String getNameFile(String nombre, String archivoExt){
		String ext = "";
		if(archivoExt != null && 0 < archivoExt.indexOf(".") ){
			ext = archivoExt.substring(archivoExt.indexOf(".")); 
		}
		StringBuffer sb = new StringBuffer();
		sb.append(nombre);
		sb.append(String.valueOf(Calendar.getInstance().getTimeInMillis()));
		sb.append(ext);
		return sb.toString();
	}
	
	/**
	 * Metodo que recibe un texto, lo formatea con mayusculas y quita lo espacios finales e iniciales
	 * @param texto
	 * @return
	 */
	public static String cambiaFormatoParrafo(String texto){
		if(texto != null && !texto.isEmpty()){
			String[] frases = texto.toLowerCase().split("\\.");
			if(frases.length > 0){
				StringBuilder sb = new StringBuilder();
				for (String str : frases) {
					if(!str.isEmpty() && str.length() > 0){
						sb.append(str.trim().substring(0,1).toUpperCase());
						sb.append(str.trim().substring(1).toLowerCase());
						sb.append(". ");
					}
				}
				if(sb.toString().length() > 0){
					texto = sb.toString().trim();
				}
			}
			return texto;
		}
		else {
			return "";
		}
	}
	
	public static String cambiaFormatoNombre(String nombre){
		if(nombre != null && !nombre.isEmpty()){
			String[] nombreCompleto = nombre.split(" ");
			if(nombreCompleto.length > 0){
				StringBuilder sb = new StringBuilder();
				for (String str : nombreCompleto) {
					if(!str.isEmpty() && str.length() > 0){
						sb.append(str.trim().substring(0,1).toUpperCase());
						sb.append(str.trim().substring(1).toLowerCase());
						sb.append(" ");
					}
				}
				if(sb.toString().length() > 0){
					nombre = sb.toString().trim();
				}
			}
			return nombre;
		}
		else{
			return "";
		}
	}
	
	
	/**
	 * Metodo que recibe un texto y solo deja su primera letra en mayuscula
	 * @param texto
	 * @return
	 */
	public static String capitalizaTexto(String texto) {
		if(texto != null) {
			StringBuilder sb = new StringBuilder();
			sb.append(Character.toUpperCase(texto.charAt(0)));
			if(texto.length() > 1) {
				sb.append(texto.substring(1));
			}			
			return sb.toString();
		}

		return null;
	}
	/**
	 * metodo que recibe un timestamp y retorna un string con la fecha en formato dd/MM/yyyy
	 * @param date
	 * @return
	 */
	
	public static String convertDateToYYYYMMDDString(Date originalDate) throws Exception{

		if(originalDate != null){
			DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
			return dateFormat.format(originalDate);
		}
		throw new Exception("La fecha Ingresada no se pudo formatear a 'yyyyMMdd'");
	}
	public static String convertDateToDDMMYYYString(Date originalDate) throws Exception{

		if(originalDate != null){
			DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			return dateFormat.format(originalDate);
		}
		return "";
	}
	
}
